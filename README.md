# protocol handler for opening shares

This program can be used to open local shares from a web application. It registers
a custom protocol `imt-open://` which then can be uses in local web applications.

The examples provide configuration for windows, chrome/chromium and edge.
## Build

```shell
cargo build # dev
cargo build --release -j12 # release build is slow
```

## Test

```shell
cargo test
```

## MSI

Downlaod the [wix toolset](https://wixtoolset.org/releases/) and unpack the binaries anywhere. `-b` shall 
point to this folder.

```shell
cargo wix --install -b"C:\Users\$USER\Downloads\wix311" 
```

Build with stdoutput of wix enabled for troubleshooting:

```shell
cargo wix --nocapture --install -b"C:\$USER\wus\Downloads\wix311" 
```

MSI Silent install:

```powershell
msiexec /i imt-open-N.N.N-x86_64.msi /qb
```

# Integration

## C / C++
```cpp
#include "stdafx.h"
#include <iostream>
#include <string>
#include "windows.h"
#include <shellapi.h>
using namespace std;

int main()
{	
    string uri = "imt-open://localhost/c$";
    cout << "Opening: \"" << uri << "\"\n";
    ShellExecuteA(NULL, "open", uri.c_str(), NULL, NULL, SW_SHOWNORMAL);
    return 0;
}
```

## C#

```csharp
using System.Diagnostics;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Process p = new Process();
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.UseShellExecute = true;
            startInfo.FileName = @"imt-open://localhost/c$";
            p.StartInfo = startInfo;
            p.Start();
        }
    }
}
```

## HTML

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>External app example</title>
  </head>
  <body>
    <a href="imt-open://localhost/c$">Launch external app: imt-open://localhost/c$</a>
  </body>
</html>
```

## Java

```java
public class Main {

    public static void main(String args[]) {
        boolean isWindows = System.getProperty("os.name")
            .toLowerCase().startsWith("windows");
        boolean isMac     = (System.getProperty("os.name").toLowerCase().indexOf("mac") >= 0);

        try {
            final Runtime rt = Runtime.getRuntime();

            if (isWindows) {
                rt.exec("cmd.exe /c start \"\" \"imt-open://localhost/c$\"");
            } else if (isMac) {
                rt.exec("open \"imt-open://localhost/c$\"");
            } else {
                rt.exec("xdg-open \"imt-open://localhost/c$\"");
            }

        } catch(Exception e) {
            System.out.println(e.toString());
            e.printStackTrace();
        }
    }
}
```

## cmd / PowerShell

```powershell
start "" "imt-open://localhost/c$"
```
