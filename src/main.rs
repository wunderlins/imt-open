#![allow(non_snake_case, non_camel_case_types)]

extern crate log;
extern crate simplelog;
extern crate url;
extern crate urlencoding;

include!("./win32.rs");

use log::{info, warn, error, debug};
use simplelog::*;
use std::str::FromStr;
//use std::fs::File;
use std::path::Path;
use url::{Url};
use urlencoding::decode;

fn main() {
    // initialize logger
    let log_level: LevelFilter = match std::env::var("RUST_LOG") {
        Ok(ll) => LevelFilter::from_str(&ll).unwrap(),
        Err(_) => LevelFilter::Info,
    };

    let mut log_path = Path::new(&std::env::var("APPDATA").unwrap().to_string()).to_path_buf();
    log_path.push("imt-open.log");

    CombinedLogger::init(
        vec![
            TermLogger::new(log_level, Config::default(), TerminalMode::Mixed, ColorChoice::Auto),
            WriteLogger::new(log_level, Config::default(), File::create(&log_path).unwrap()),
        ]
    ).unwrap();
    info!("log_path {}", log_path.display().to_string());

    // we expect parameter 1 to be an url
    let args: Vec<String> = std::env::args().collect();
    let url_str = match args.get(1) {
        Some(url) => url,
        None      => { 
            error!("Prameter 1 missing");
            std::process::exit(1);
        }
    };
    info!("url: {}", url_str);

    let url = match Url::parse(url_str) {
        Ok(u)  => u,
        Err(e) => {
            error!("Failed to parse url: {}. {}", url_str, e);
            std::process::exit(2);
        }
    };
    debug!("{:#?}", url);

    let mut path = "".to_string();
    if url.host_str() != None {
        let host = decode(url.host_str().unwrap()).unwrap();
        let len = host.len();
        if len > 1 && host.chars().last().unwrap() != ':' {
            path.push_str("//");
        }
        path.push_str(&host);
        // one char long host part, assuming windows drive letter
        if len < 2 {
            path.push_str(&":");
        }
    }
    if url.path() != "" {
        path.push_str(&decode(url.path()).unwrap());
    }
    if path == "" {
        path.push_str("/");
        warn!("Empty path in url, setting \"/\" as path.");
    }
    path = path.replace("/", "\\");
    info!("opening {:#?}", path);

    let _p = CreateProcess(
        None, //Some(&OsStr::new("explorer.exe")),
        &OsStr::new(&format!("explorer.exe {}", path)),
        &None,
        &None,
        true,
        0,
        None,
        None,
        None,
        0,
    );
}
